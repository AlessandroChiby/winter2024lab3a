public class Student {
    String name;
    int grade;
    String program;

    public Student (String name, int grade, String program) {
        this.name = name;
        this.grade = grade;
        this.program = program;
    }
    public void study() {
        grade+=5;
        if (grade > 100) grade = 100;
    }
    public void party() {
        if (program.equals("Psychology")) {
            System.out.println("Pshychology students are too boring to party");
        } else {
            grade-=5;
            if (grade < 0) grade = 0;
        }
    }
}